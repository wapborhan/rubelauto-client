import { useState } from "react";

const AddSale = () => {
  const [saleInfo, setSaleInfo] = useState();

  const handleSubmit = (e) => {
    e.preventDefault();
    const form = e.target;

    const saledate = form.saledate.value;
    const cardno = form.cardno.value;
    const showroom = form.showroom.value;
    const name = form.name.value;
    const coname = form.coname.value;
    const address = form.address.value;
    const number = form.number.value;
    const location = form.location.value;
    const media = form.media.value;
    const productType = form.productType.value;
    const model = form.model.value;
    const engine = form.engine.value;
    const chassis = form.chassis.value;
    const saleprice = form.saleprice.value;
    const dpamount = form.dpamount.value;
    const term = form.term.value;
    const percentage = form.percentage.value;
    const insdate = form.insdate.value;
    const hireprice = form.hireprice.value;
    const insamount = form.insamount.value;
    const conddate = form.conddate.value;
    const condamount = form.condamount.value;

    const inputData = {
      saledate,
      cardno,
      showroom,
      customerInfo: { name, coname, address, number, location, media },
      productInfo: { productType, model, engine, chassis },
      accountInfo: {
        saleprice,
        dpamount,
        term,
        percentage,
        insdate,
        hireprice,
        insamount,
        conddate,
        condamount,
      },

      installment: {},
    };
    console.log(inputData);

    // fetch("http://localhost:3300/add-coffee", {
    //   method: "POST",
    //   headers: { "Content-Type": "application/json" },
    //   body: JSON.stringify(inputData),
    // })
    //   .then((response) => response.json())
    //   .then((data) => {
    //     if (data.insertedId) {
    //       alert("Coffe Added");
    //       setCoffee(data);
    //       form.reset();
    //     }
    //   });
  };

  // console.log(coffee);
  return (
    <div className="addcoffe">
      <div className="back">{/* <BackToHomePage /> */}</div>
      <div className="sect  py-4 w-full mx-auto">
        <div className="content space-y-5">
          <h2 className="text-center text-3xl"> Add Sale</h2>
        </div>

        <form onSubmit={handleSubmit}>
          <div className="form  ">
            <div className="start flex gap-5 justify-between">
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Sale Date</span>
                </label>
                <input
                  type="date"
                  name="saledate"
                  placeholder="Enter Sale Date"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Card Number</span>
                </label>
                <input
                  type="text"
                  name="cardno"
                  placeholder="Enter Card Number"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Show Room</span>
                </label>
                <select
                  className="select select-bordered w-full "
                  name="showroom"
                >
                  <option disabled selected>
                    Showroom List
                  </option>
                  <option>Bheramara</option>
                  <option>Bajaj</option>
                  <option>Mirpur</option>
                  <option>Dorga</option>
                  <option>Kushtia</option>
                  <option>Dashuria</option>
                  <option>Pabna</option>
                  <option>Arambaria</option>
                  <option>Meherpur</option>
                  <option>Chuadanga</option>
                  <option>Alamdanga</option>
                </select>
              </div>
            </div>
            <div className="frist flex gap-5 justify-between">
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Customer Name</span>
                </label>
                <input
                  type="text"
                  name="name"
                  placeholder="Enter Customer Name"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">
                    Father/Husband Name
                  </span>
                </label>
                <input
                  type="text"
                  name="coname"
                  placeholder="Enter Father/Husband Name"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Address</span>
                </label>
                <input
                  type="text"
                  name="address"
                  placeholder="Enter Customer Address"
                  className="input input-bordered w-full"
                />
              </div>
            </div>
            <div className="second flex gap-5 justify-between">
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Contact No.</span>
                </label>
                <input
                  type="number"
                  name="number"
                  placeholder="Enter Mobile Number"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Location Mark</span>
                </label>
                <input
                  type="text"
                  name="location"
                  placeholder="Enter Customer Location Mark"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Media & Number</span>
                </label>
                <input
                  type="text"
                  name="media"
                  placeholder="Enter Media name - Mobile No."
                  className="input input-bordered w-full"
                />
              </div>
            </div>
            <div className="third flex gap-5 justify-between">
              <div className="form-control w-6/12">
                <label className="label">
                  <span className="label-text font-bold">Product Type</span>
                </label>
                <select
                  name="productType"
                  className="select select-bordered w-full "
                >
                  <option disabled selected>
                    Product Type
                  </option>
                  <option>Motorcycle</option>
                  <option>LPG</option>
                  <option>Easy Bike</option>
                  <option>Battery</option>
                </select>
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Model</span>
                </label>
                <input
                  type="text"
                  name="model"
                  placeholder="Enter Product Model"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Engine No.</span>
                </label>
                <input
                  type="text"
                  name="engine"
                  placeholder="Enter Engine No"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold"> Chassis No.</span>
                </label>
                <input
                  type="text"
                  name="chassis"
                  placeholder="Enter Chassis No"
                  className="input input-bordered w-full"
                />
              </div>
            </div>
            <div className="fourth flex gap-5 justify-between">
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Sale Price</span>
                </label>
                <input
                  type="number"
                  name="saleprice"
                  placeholder="Enter Sale Price"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Down Payment</span>
                </label>
                <input
                  type="number"
                  name="dpamount"
                  placeholder="Enter Down Payment"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Terms</span>
                </label>
                <input
                  type="number"
                  name="term"
                  placeholder="Enter Terms"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Percentage</span>
                </label>
                <input
                  type="number"
                  name="percentage"
                  placeholder="Enter Percentage"
                  className="input input-bordered w-full"
                />
              </div>
            </div>
            <div className="fifth flex gap-5 justify-between">
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Installment Date</span>
                </label>
                <input
                  type="date"
                  name="insdate"
                  placeholder="Enter Installment Date"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Hire Price</span>
                </label>
                <input
                  type="number"
                  name="hireprice"
                  placeholder="Enter Hire Price"
                  className="input input-bordered w-full"
                />
              </div>

              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">
                    Installment Amount
                  </span>
                </label>
                <input
                  type="number"
                  name="insamount"
                  placeholder="Enter Terms"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Condition Date</span>
                </label>
                <input
                  type="date"
                  name="conddate"
                  placeholder="Enter Percentage"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Condition Amount</span>
                </label>
                <input
                  type="number"
                  name="condamount"
                  placeholder="Enter Percentage"
                  className="input input-bordered w-full"
                />
              </div>
            </div>

            <div className="submit">
              <input
                type="submit"
                value="Submit"
                className="rounded-lg font-h2 mt-4 border-2-[#331A15] bg-[#D2B48C] w-full p-3 font-bold text-[18px] text-[#331A15] cursor-pointer"
              />
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AddSale;
