import "../details/dsds.css";

const CusInfo = ({ selectCustomer }) => {
  const { cardno, saledate, showroom, customerInfo, productInfo, saleInfo } =
    selectCustomer;
  return (
    <table className="table table-bordered border-dark info w-100">
      <thead>
        <tr>
          <th scope="col" colSpan="6" className="text-center">
            Customer Info
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td className="tdwd1">Card No.</td>
          <td className="tdwd">{cardno}</td>
          <td className="tdwd1">Sale Date </td>
          <td className="tdwd">{saledate}</td>
          <td className="tdwd1">Showroom </td>
          <td className="tdwd">{showroom}</td>
        </tr>
        <tr>
          <td className="tdwd1">Customer Name</td>
          <td className="tdwd">{customerInfo?.name}</td>
          <td className="tdwd1">Media & Number </td>
          <td className="tdwd">{customerInfo?.media}</td>
          <td className="tdwd1">Model </td>
          <td className="tdwd">{productInfo?.model}</td>
        </tr>
        <tr>
          <td className="tdwd1">CO Name</td>
          <td className="tdwd">{customerInfo?.coname}</td>
          <td className="tdwd1">Guarantor 1_Name & No. </td>
          <td className="tdwd"></td>
          <td className="tdwd1">Color / Battery</td>
          <td className="tdwd">{productInfo?.colorbattery}</td>
        </tr>
        <tr>
          <td className="tdwd1">Mobile</td>
          <td className="tdwd">{customerInfo?.number}</td>
          <td className="tdwd1">Guarantor 2_Name & No. </td>
          <td className="tdwd"></td>
          <td className="tdwd1">Engine No. </td>
          <td className="tdwd">{productInfo?.engine}</td>
        </tr>
        <tr>
          <td className="tdwd1">Address</td>
          <td className="tdwd">{customerInfo?.address}</td>
          <td className="tdwd1">Location Mark </td>
          <td className="tdwd">{customerInfo?.location}</td>
          <td className="tdwd1">Chassis No. </td>
          <td className="tdwd">{productInfo?.chassis}</td>
        </tr>
      </tbody>
    </table>
  );
};

export default CusInfo;
