import { useEffect, useState } from "react";
import { useLoaderData, useParams } from "react-router-dom";
import CusInfo from "./CusInfo";
import CusAccountInfo from "./CusAccountInfo";

const CustomerDetails = () => {
  const [selectCustomer, setSelectCustomer] = useState(null);
  const event = useLoaderData();
  const { cardNo } = useParams();

  useEffect(() => {
    const selectedcustomer = event?.find(
      (customer) => customer?.cardno === cardNo
    );
    setSelectCustomer(selectedcustomer);
  }, [event, cardNo]);
  return (
    <div>
      <div className="customer">
        <div className="company text-center space-y-5">
          <h2 className="text-4xl font-bold">Customer Ledger</h2>
          <h2 className="text-3xl">Rubel Auto</h2>
        </div>
        <div className="customer-info">
          {selectCustomer ? <CusInfo selectCustomer={selectCustomer} /> : ""}
          {selectCustomer ? (
            <CusAccountInfo selectCustomer={selectCustomer} />
          ) : (
            ""
          )}
        </div>
      </div>
    </div>
  );
};

export default CustomerDetails;
