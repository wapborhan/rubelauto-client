const CusAccountInfo = ({ selectCustomer }) => {
  const { installment } = selectCustomer;
  console.log(selectCustomer);
  return (
    <table className="table table-bordered border-dark">
      <thead>
        <tr>
          <th scope="col">SL</th>
          <th scope="col">Date</th>
          <th scope="col">Amount </th>
          <th scope="col">Voucher No.</th>
          <th scope="col">Received By</th>
          <th scope="col">Notes</th>
        </tr>
      </thead>
      <tbody>
        {installment?.map((ins, idx) => {
          return (
            <tr key={idx}>
              <th scope="row">{idx + 1}</th>
              <td>{ins?.date}</td>
              <td>{ins?.amount}</td>
              <td>{ins?.voucher}</td>
              <td>{ins?.receiver}</td>
              <td>{ins?.coments}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default CusAccountInfo;
