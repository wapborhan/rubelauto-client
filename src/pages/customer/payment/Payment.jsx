import { useLoaderData, useNavigate } from "react-router-dom";

const Payment = () => {
  const today = new Date();

  const navigate = useNavigate();

  const customer = useLoaderData();
  const { cardno } = customer;
  console.log(customer);

  const handleSubmit = (e) => {
    e.preventDefault();
    const form = e.target;
    const date = form.date.value;
    const amount = form.amount.value;
    const voucher = form.voucher.value;
    const receiver = form.receiver.value;
    const coments = form.coments.value;

    const installmentData = {
      date,
      amount,
      voucher,
      receiver,
      coments,
    };

    console.log(installmentData);

    fetch(`http://localhost:3300/payment/${cardno}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(installmentData),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.modifiedCount > 0) {
          alert("Installment Addeded Complited");
          navigate(`/customer/${cardno}`);
        } else if (data.modifiedCount === 0) {
          alert("Nothing Modified");
        }
      });
  };

  return (
    <div className="container max-w-6xl mx-auto">
      <div className="sect py-4 w-full mx-auto">
        <div className="content space-y-5">
          <h2 className="text-center text-3xl"> Add Installment</h2>
        </div>
        <div className="h2">{customer?.cardno}</div>

        <form onSubmit={handleSubmit}>
          <div className="form  mx-40 my-10 space-y-4">
            <div className="frist flex gap-5 justify-between">
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Date</span>
                </label>
                <input
                  type="text"
                  name="date"
                  placeholder="Today"
                  defaultValue={today}
                  className="input input-bordered w-full"
                  disabled
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Amount</span>
                </label>
                <input
                  type="number"
                  name="amount"
                  placeholder="Installment Amount"
                  className="input input-bordered w-full"
                />
              </div>
            </div>
            <div className="second flex gap-5 justify-between">
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Voucher</span>
                </label>
                <input
                  type="text"
                  name="voucher"
                  placeholder="Voucher Number"
                  className="input input-bordered w-full"
                />
              </div>
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Receiver</span>
                </label>
                <select name="receiver" className="input input-bordered w-full">
                  <option value="Sagor">Sagor</option>
                  <option value="Nadim">Nadim</option>
                  <option value="Sujon">Sujon</option>
                </select>
              </div>
            </div>
            <div className="third flex gap-5 justify-between">
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text font-bold">Coments</span>
                </label>
                <textarea
                  type="text"
                  name="coments"
                  placeholder="Coments"
                  className="input input-bordered w-full h-20"
                />
              </div>
            </div>

            <div className="submit">
              <input
                type="submit"
                value="Add Installment"
                className="rounded-lg font-h2 border-2-[#331A15] bg-[#D2B48C] w-full p-3 font-bold text-[18px] text-[#331A15] cursor-pointer"
              />
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Payment;
