import { useState, useEffect, useRef } from "react";
// import { classNames } from "primereact/utils";
import { FilterMatchMode } from "primereact/api";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import { Dropdown } from "primereact/dropdown";
import { Tag } from "primereact/tag";
import { TriStateCheckbox } from "primereact/tristatecheckbox";
import "./cus.css";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import { NavLink } from "react-router-dom";

export default function BasicFilterDemo() {
  const dt = useRef(null);
  const [customers, setCustomers] = useState(null);
  const [filters, setFilters] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
    cardno: { value: null, matchMode: FilterMatchMode.CONTAINS },
    saledate: { value: null, matchMode: FilterMatchMode.CONTAINS },
    "customerInfo.name": {
      value: null,
      matchMode: FilterMatchMode.STARTS_WITH,
    },
    "customerInfo.number": {
      value: null,
      matchMode: FilterMatchMode.STARTS_WITH,
    },
    representative: { value: null, matchMode: FilterMatchMode.IN },
    showroom: { value: null, matchMode: FilterMatchMode.EQUALS },
    verified: { value: null, matchMode: FilterMatchMode.CONTAINS },
  });
  const [loading, setLoading] = useState(true);
  const [globalFilterValue, setGlobalFilterValue] = useState("");
  const [statuses] = useState([
    "Bheramara",
    "Bajaj",
    "Mirpur",
    "Dorga",
    "Kushtia",
  ]);

  useEffect(() => {
    fetch("http://localhost:3300/customers")
      .then((res) => res.json())
      .then((data) => {
        setCustomers(getCustomers(data));
        setLoading(false);
      });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const getCustomers = (data) => {
    return [...(data || [])].map((d) => {
      d.date = new Date(d.date);

      return d;
    });
  };

  const onGlobalFilterChange = (e) => {
    const value = e.target.value;
    let _filters = { ...filters };

    _filters["global"].value = value;

    setFilters(_filters);
    setGlobalFilterValue(value);
  };

  const cols = [
    { field: "cardno", header: "Card No" },
    { field: "country", header: "Country" },
    { field: "representative", header: "Agent" },
    { field: "status", header: "Status" },
  ];

  const exportColumns = cols.map((col) => ({
    title: col.header,
    dataKey: col.field,
  }));

  const exportCSV = (selectionOnly) => {
    dt.current.exportCSV({ selectionOnly });
  };

  const exportPdf = () => {
    import("jspdf").then((jsPDF) => {
      import("jspdf-autotable").then(() => {
        const doc = new jsPDF.default(0, 0);

        doc.autoTable(exportColumns, customers);
        doc.save("products.pdf");
      });
    });
  };
  const exportExcel = () => {
    import("xlsx").then((xlsx) => {
      const worksheet = xlsx.utils.json_to_sheet(customers);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ["data"] };
      const excelBuffer = xlsx.write(workbook, {
        bookType: "xlsx",
        type: "array",
      });

      saveAsExcelFile(excelBuffer, "products");
    });
  };

  const saveAsExcelFile = (buffer, fileName) => {
    import("file-saver").then((module) => {
      if (module && module.default) {
        let EXCEL_TYPE =
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
        let EXCEL_EXTENSION = ".xlsx";
        const data = new Blob([buffer], {
          type: EXCEL_TYPE,
        });

        module.default.saveAs(
          data,
          fileName + "_export_" + new Date().getTime() + EXCEL_EXTENSION
        );
      }
    });
  };

  const renderHeader = () => {
    return (
      <div className="flex justify-between">
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText
            value={globalFilterValue}
            onChange={onGlobalFilterChange}
            placeholder="Keyword Search"
          />
        </span>
        <div className="flex align-items-center  justify-between gap-2">
          <Button
            type="button"
            icon="pi pi-file"
            rounded
            onClick={() => exportCSV(false)}
            data-pr-tooltip="CSV"
            className="bg-warning p-3"
          >
            CSV
          </Button>
          <Button
            type="button"
            icon="pi pi-file-excel"
            severity="success"
            rounded
            onClick={exportExcel}
            data-pr-tooltip="XLS"
            className="bg-warning p-3"
          >
            XLS
          </Button>
          <Button
            type="button"
            icon="pi pi-file-pdf"
            severity="warning"
            rounded
            onClick={exportPdf}
            data-pr-tooltip="PDF"
            className="bg-warning p-3"
          >
            PDF
          </Button>
        </div>
      </div>
    );
  };

  const countryBodyTemplate = (rowData) => {
    return (
      <div className="flex align-items-center gap-2">
        <span>{rowData.customerInfo.name}</span>
      </div>
    );
  };
  const numberBodyTemplate = (rowData) => {
    return (
      <div className="flex align-items-center gap-2">
        <span>{rowData.customerInfo.number}</span>
      </div>
    );
  };
  const modelBodyTemplate = (rowData) => {
    return (
      <div className="flex align-items-center gap-2">
        <span>{rowData.productInfo.model}</span>
      </div>
    );
  };
  const engineBodyTemplate = (rowData) => {
    return (
      <div className="flex align-items-center gap-2">
        <span>{rowData.productInfo.engine}</span>
      </div>
    );
  };

  const statusBodyTemplate = (rowData) => {
    return <Tag value={rowData.showroom} />;
  };

  const statusItemTemplate = (option) => {
    return <Tag value={option} />;
  };

  const verifiedBodyTemplate = (rowData) => {
    return (
      <div className="flex gap-2">
        <NavLink
          to={`/customer/${rowData?.cardno}`}
          className="text-indigo-600 hover:text-indigo-900 focus:outline-none focus:underline"
        >
          Show
        </NavLink>
        <NavLink
          to={`/payment/${rowData?.cardno}`}
          className="text-indigo-600 hover:text-indigo-900 focus:outline-none focus:underline"
        >
          Payment
        </NavLink>
      </div>
    );
  };

  const statusRowFilterTemplate = (options) => {
    return (
      <Dropdown
        value={options.value}
        options={statuses}
        onChange={(e) => options.filterApplyCallback(e.value)}
        itemTemplate={statusItemTemplate}
        placeholder="Select One"
        className="p-column-filter"
        showClear
        style={{ minWidth: "7rem" }}
      />
    );
  };

  const verifiedRowFilterTemplate = (options) => {
    return (
      <TriStateCheckbox
        value={options.value}
        onChange={(e) => options.filterApplyCallback(e.value)}
      />
    );
  };

  const header = renderHeader();

  return (
    <div className="card">
      <DataTable
        ref={dt}
        value={customers}
        paginator
        rows={10}
        dataKey="id"
        filters={filters}
        filterDisplay="row"
        loading={loading}
        globalFilterFields={[
          "cardno",
          "customerInfo.name",
          "customerInfo.number",
          "productInfo.engine",
        ]}
        header={header}
        emptyMessage="No customers found."
      >
        <Column
          field="cardno"
          header="Card No."
          filter
          showFilterMenu={false}
          filterPlaceholder="Search"
          style={{ minWidth: "8rem" }}
        />
        <Column
          field="showroom"
          header="Showroom"
          showFilterMenu={false}
          filterMenuStyle={{ width: "7rem" }}
          style={{ minWidth: "7rem" }}
          body={statusBodyTemplate}
          filter
          filterElement={statusRowFilterTemplate}
        />
        <Column
          field="saledate"
          header="Sale date"
          filter
          showFilterMenu={false}
          filterPlaceholder="Sale Date"
          style={{ minWidth: "8rem" }}
        />
        <Column
          header="Name"
          filterField="customerInfo.name"
          style={{ minWidth: "10rem" }}
          body={countryBodyTemplate}
          filter
          showFilterMenu={false}
          filterPlaceholder="Name"
        />
        <Column
          header="Mobile"
          filterField="customerInfo.number"
          style={{ minWidth: "9rem" }}
          body={numberBodyTemplate}
          filter
          showFilterMenu={false}
          filterPlaceholder="Number"
        />
        <Column
          header="Model"
          filterField="productInfo.model"
          style={{ minWidth: "8rem" }}
          body={modelBodyTemplate}
          filter
          showFilterMenu={false}
          filterPlaceholder="Model"
        />
        <Column
          header="Engine No."
          filterField="productInfo.engine"
          style={{ minWidth: "7rem" }}
          body={engineBodyTemplate}
          filter
          showFilterMenu={false}
          filterPlaceholder="Engine"
        />

        <Column
          field="verified"
          header="Action"
          dataType="boolean"
          style={{ minWidth: "6rem" }}
          body={verifiedBodyTemplate}
          filter
          filterElement={verifiedRowFilterTemplate}
        />
      </DataTable>
    </div>
  );
}
