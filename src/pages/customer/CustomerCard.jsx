import { NavLink } from "react-router-dom";

const CustomerCard = ({ customer }) => {
  const { customerInfo, cardno, saledate, productInfo, engine, showroom } =
    customer;
  // console.log(customer);
  return (
    <tr>
      <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
        <input
          className="form-checkbox h-4 w-4 text-indigo-600 transition duration-150 ease-in-out"
          type="checkbox"
        />
      </td>
      <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
        <div className="text-sm leading-5 text-gray-900">{cardno}</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
        <div className="text-sm leading-5 text-gray-900">{showroom}</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
        <div className="text-sm leading-5 text-gray-900">{saledate}</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
        <div className="flex items-center">
          {customerInfo?.name}
          <div className="ml-4">
            <div className="text-sm leading-5 font-medium text-gray-900">s</div>
          </div>
        </div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
        <div className="text-sm leading-5 text-gray-900">
          {customerInfo?.coname}
        </div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
        <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
          {customerInfo?.number}
        </span>
      </td>
      {/* <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
        {customerInfo?.address}
      </td> */}
      <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
        {productInfo?.model}
      </td>
      <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
        {productInfo?.engine}
      </td>
      <td className="px-6 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium">
        <NavLink
          to={`/customer/${cardno}`}
          className="text-indigo-600 hover:text-indigo-900 focus:outline-none focus:underline"
        >
          Show
        </NavLink>
      </td>
    </tr>
  );
};

export default CustomerCard;
