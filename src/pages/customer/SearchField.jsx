const SearchField = ({ cardData, mobileData }) => {
  // const handleInputChange = (e) => {
  //   const { name, value } = e.target;
  //   onSearch({ [name]: value });
  // };

  return (
    <>
      <th>
        <input
          type="text"
          name="card"
          placeholder="Card"
          className="input input-bordered w-full max-w-xs"
          onChange={(e) => cardData(e.target.value)}
        />
      </th>
      <th>
        {" "}
        <input
          type="text"
          name="sale"
          placeholder="Sale Date"
          className="input input-bordered w-full max-w-xs"
          // onChange={handleInputChange}
        />
      </th>
      <th>
        {" "}
        <input
          type="text"
          name="name"
          placeholder="Name"
          className="input input-bordered w-full max-w-xs"
          // onChange={handleInputChange}
        />
      </th>
      <th>
        {" "}
        <input
          type="text"
          name="C/O"
          placeholder="C/O"
          className="input input-bordered w-full max-w-xs"
          // onChange={handleInputChange}
        />
      </th>
      <th>
        {" "}
        <input
          type="text"
          name="mobile"
          placeholder="Mobile"
          className="input input-bordered w-full max-w-xs"
          onChange={(e) => mobileData(e.target.value)}
        />
      </th>
      <th>
        {" "}
        <input
          type="text"
          name="address"
          placeholder="Address"
          className="input input-bordered w-full max-w-xs"
          // onChange={handleInputChange}
        />
      </th>
      <th>
        {" "}
        <input
          type="text"
          name="model"
          placeholder="Model"
          className="input input-bordered w-full max-w-xs"
          // onChange={handleInputChange}
        />
      </th>
      <th>
        {" "}
        <input
          type="text"
          name="engine"
          placeholder="Engine"
          className="input input-bordered w-full max-w-xs"
          // onChange={handleInputChange}
        />
      </th>
    </>
  );
};

export default SearchField;
