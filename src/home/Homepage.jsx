import Header from "./header/Header";
import "../assets/css/responsive.css";
import "../assets/css/style.css";

const Homepage = () => {
  return (
    <div>
      <Header />
    </div>
  );
};

export default Homepage;
