const TopBar = () => {
  return (
    <div className="ts-top-bar">
      <div className="top-bar-angle">
        <div className="container">
          <div className="flex justify-end">
            <div className="col-span-6 col-md-4"></div>
            <div className="col-span-4 col-md-5 flex gap-4">
              <div className="top-bar-event ts-top">
                <i className="icon icon-clock"></i>
                <span>We'are Open: Sat - Thr 9:00am - 08:00pm</span>
              </div>
            </div>
            <div className="col-lg-2 col-md-3 text-right">
              <div className="top-bar-social-icon ml-auto">
                <ul>
                  <li>
                    <a href="#">
                      <i className="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-google-plus"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-instagram"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TopBar;
