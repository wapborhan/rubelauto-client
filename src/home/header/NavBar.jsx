import { NavLink } from "react-router-dom";
import logo from "../../assets/images/logo/logo.png";

const NavBar = () => {
  return (
    <header className="ts-header header-default">
      <div className="ts-logo-area">
        <div className="container max-w-7xl mx-auto">
          <div className="row align-items-center flex justify-between">
            <div className="col-md-4 col-sm-4">
              <a className="ts-logo" href="index.html">
                <img src={logo} alt="logo" className="h-12" />
              </a>
            </div>
            <div className="col-md-8 col-sm-8 float-right">
              <ul className="top-contact-infos flex items-center">
                <li>
                  <span>
                    <i className="icon icon-phone3"></i>
                  </span>
                  <div className="info-wrapper">
                    <p className="info-title">Call us</p>
                    <p className="info-subtitle">+880 1719 033 0880</p>
                  </div>
                </li>
                <li>
                  <span>
                    <i className="icon icon-envelope"></i>
                  </span>
                  <div className="info-wrapper">
                    <p className="info-title">Send us mail</p>
                    <p className="info-subtitle">
                      <a
                        href="mailto:mail.rubelauto@gmail.com"
                        className="__cf_email__"
                      >
                        mail.rubelauto@gmail.com
                      </a>
                    </p>
                  </div>
                </li>
                <li>
                  <NavLink className="btn btn-primary" to="/dashboard">
                    Dashboard
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div className="header-angle">
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light">
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div
              className="collapse navbar-collapse justify-content-end ts-navbar"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav flex justify-center">
                <li className="nav-item dropdown active">
                  <a
                    className="nav-link active"
                    href="#"
                    data-toggle="dropdown"
                  >
                    Home
                  </a>
                </li>
                <li className="nav-item dropdown">
                  <a className="nav-link" href="#" data-toggle="dropdown">
                    About
                    <span className="ts-indicator">
                      <i className="fa fa-angle-down"></i>
                    </span>
                  </a>
                </li>
                <li className="nav-item dropdown">
                  <a className="nav-link" href="#" data-toggle="dropdown">
                    Services
                    <span className="ts-indicator">
                      <i className="fa fa-angle-down"></i>
                    </span>
                  </a>
                </li>

                {/* <!-- <li className="nav-item dropdown">
                      <a className="nav-link" href="#" data-toggle="dropdown">
                      Shop
                      <span className="ts-indicator"><i className="fa fa-angle-down"></i></span>
                      </a>
                   </li> --> */}
                <li className="nav-item dropdown">
                  <a className="nav-link" href="#" data-toggle="dropdown">
                    Blog
                    <span className="ts-indicator">
                      <i className="fa fa-angle-down"></i>
                    </span>
                  </a>
                </li>

                <li className="nav-item dropdown">
                  <a className="nav-link" href="contact.html">
                    Contact
                  </a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </header>
  );
};

export default NavBar;
