import NavBar from "./NavBar";
import TopBar from "./TopBar";

const Header = () => {
  return (
    <div>
      <TopBar />
      <NavBar />
    </div>
  );
};

export default Header;
