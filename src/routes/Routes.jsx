import { createBrowserRouter } from "react-router-dom";
import Root from "../layout/Root";
import HomePage from "../home/Homepage";
import Dashboard from "../pages/dashboard/Dashboard";
import AddSale from "../pages/sale/AddSale";
import Customer from "../pages/customer/Customer";
import CustomerDetails from "../pages/customer/details/CustomerDetails";
import Payment from "../pages/customer/payment/Payment";

const routes = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
    errorElement: "Error",
  },
  {
    path: "/",
    element: <Root />,
    children: [
      {
        path: "/dashboard",
        element: <Dashboard />,
      },
      {
        path: "/add-sale",
        element: <AddSale />,
      },
      {
        path: "/customer",
        element: <Customer />,
        loader: () => fetch("http://localhost:3300/customers"),
      },
      {
        path: "/customer/:cardNo",
        element: <CustomerDetails />,
        loader: () => fetch("http://localhost:3300/customers"),
      },
      {
        path: "/payment/:cardNo",
        element: <Payment />,
        loader: ({ params }) =>
          fetch(`http://localhost:3300/customers/${params.cardNo}`),
      },
    ],
  },
]);

export default routes;
