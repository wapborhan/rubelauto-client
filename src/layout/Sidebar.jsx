import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import {
  ImCoinDollar,
  ImStatsBars,
  ImEnter,
  ImExit,
  ImCog,
} from "react-icons/im";

const Sidebar = () => {
  return (
    <Fragment>
      <div className="logo-details">
        <ImCoinDollar />
        <span className="logo_name">Rubel AUto</span>
      </div>
      <ul className="nav-links">
        <li>
          <NavLink to="/">
            <i className="fas fa-exclamation-triangle text-white">
              <ImStatsBars />
            </i>
            <span className="links_name">Home</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/dashboard">
            <i className="fas fa-exclamation-triangle text-white">
              <ImStatsBars />
            </i>
            <span className="links_name">Dashboard</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/add-sale">
            <i className="fas fa-exclamation-triangle text-white">
              <ImEnter />
            </i>
            <span className="links_name">Add Sales</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/customer">
            <i className="fas fa-exclamation-triangle text-white">
              <ImEnter />
            </i>
            <span className="links_name">Customer</span>
          </NavLink>
        </li>

        {/* <li className="log_out">
          <NavLink to="/setting">
            <i className="fas fa-exclamation-triangle text-white">
              <ImCog />
            </i>
            <span className="links_name">About</span>
          </NavLink>
        </li> */}
      </ul>
    </Fragment>
  );
};

export default Sidebar;
