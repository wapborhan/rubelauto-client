import { useState } from "react";
import { Outlet } from "react-router-dom";
import Sidebar from "./Sidebar";
import { FaBars, FaUserCircle, FaAlignLeft } from "react-icons/fa";

const Root = () => {
  const [isActive, setActive] = useState("false");
  const handleToggle = () => {
    setActive(!isActive);
  };
  return (
    <div>
      <div id="wrapper">
        <div
          className={
            isActive
              ? "sidebars bg-slate-900 text-light  "
              : "sidebars bg-slate-900 active"
          }
        >
          <Sidebar />
        </div>
        <div id="content-wrapper" className="flex flex-col">
          <div id="content">
            <nav
              id="topbar"
              className="topbar navbar-expand navbar-light bg-white  mb-4 static-top shadow flex justify-between px-4 items-center"
            >
              <div
                className="sidebars-button text-dark "
                onClick={handleToggle}
              >
                {isActive ? <FaBars /> : <FaAlignLeft />}
                {/* <FaBars /> */}
                <div className={isActive ? "dashboard" : "dashboard-show"}>
                  Rubel Auto
                </div>
              </div>

              <h2 id="nameTitle" className="text-center text-dark">
                Rubel Auto
              </h2>
              <div className="account ">
                <div className="dropdown dropdown-end">
                  <label tabIndex={0} className=" btn-circle avatar">
                    <div className="w-10 rounded-full">
                      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIY3mOQ9RyRtyy1IF5OA5OKVxxo9_cCAyTlAHpbAhtVenVNPmXJXQnWa_us9mOXqf9ivA&usqp=CAU" />
                    </div>
                  </label>
                  <ul
                    tabIndex={0}
                    className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52"
                  >
                    <li>
                      <a className="justify-between">
                        Profile
                        <span className="badge">New</span>
                      </a>
                    </li>
                    <li>
                      <a>Settings</a>
                    </li>
                    <li>
                      <a>Logout</a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
            <div className="container-fluid topbarpt">
              <Outlet />
            </div>
          </div>
          {/* <Footer /> */}
        </div>
      </div>
    </div>
  );
};

export default Root;
